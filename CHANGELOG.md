## Harvest Lights Changelog

#### 1.4

* Added support for even more T_D flora and lights

[Download Link](https://gitlab.com/modding-openmw/harvest-lights/-/packages/22841996)

#### 1.3

* Added support for more T_D flora and lights

[Download Link](https://gitlab.com/modding-openmw/harvest-lights/-/packages/21429917)

#### 1.2

* Fixed a problem with case sensitivity of IDs
* Updated the SV localization

[Download Link](https://gitlab.com/modding-openmw/harvest-lights/-/packages/20782001)

#### 1.1

* Added an "extra scan" system that uses a user-configurable value (default 20 units):
  * If a light is found within the extra range, it is checked for any harvestables within the normal range
  * If that light has no harvestables within the normal range, it is considered attached to this one
  * Example cluster affected by this that was "fixed" by simply raising the range:
    * `player->position -18308.468750 -63039.789062 375.079742 260`
* Addded SV localization (thanks Lysol!)
* Implemented an interface that allows external mods to add support for more IDs; this is documented in the README and with an example-addon that's included.
* Fixed a problem that prevented two clusters in close proximity to one another from being detected properly (with Tamriel Rebuilt installed, it's these clusters: `player->position 89358.671875 -144307.234375 1773.982056 340`).
* Also made the max detect range user-configurable via the script settings menu, with a caveat about possible breakage.

[Download Link](https://gitlab.com/modding-openmw/harvest-lights/-/packages/20679917)

#### 1.0

* Initial release

[Download Link](https://gitlab.com/modding-openmw/harvest-lights/-/packages/20556827)
