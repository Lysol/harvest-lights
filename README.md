# Harvest Lights

Disables lights around harvestable objects and re-enables them after the object has grown back. Dynamic, compatible with everything. **OpenMW 0.49 or newer required!**

This is intended to be used with [Graphic Herbalism - MWSE and OpenMW Edition](https://www.nexusmods.com/morrowind/mods/46599) and other mods which provide assets that utilize the "graphic herbalism" harvesting feature. Supports the latest release of [Tamriel Rebuilt](https://www.tamriel-rebuilt.org/).

#### Credits

**Author**: johnnyhostile

**Lua Help**: gnounc

**Localizations**:

* **EN**: johnnyhostile

* **SV**: Lysol

**Special Thanks**:

* Ronik for help with TD IDs
* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/harvest-lights/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/harvest-lights)

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/harvest-lights/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Lighting\harvest-lights

        # Linux
        /home/username/games/OpenMWMods/Lighting/harvest-lights

        # macOS
        /Users/username/games/OpenMWMods/Lighting/harvest-lights

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Lighting\harvest-lights"`)
1. Add `content=harvest-lights.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher

Please note that this will not disable lights near objects you've harvested in an existing save (see planned features below).

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/harvest-lights/-/issues/new) for bug reports or feature requests
* Email: `harvest-lights at modding-openmw dot com`
<!-- * Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/#TODO?tab=posts) -->

#### How It Works

This pure Lua mod tracks when you have harvested certain Alchemy ingredient containers. When a cluster of these have been harvested leaving only a light object behind, this light is then disabled. When the ingredients respawn, the light is re-enabled.

Only the supported harvestable object IDs and their corresponding light object IDs are hardcoded, everything else is dynamic. The range at which objects and lights detect one another is configurable, but it's not advised to change that unless you're troubleshooting some problem.

#### Adding Support For More IDs

It's possible to extend the IDs that this mod supports without modifying its source directly. An "addon" mod can be created that uses the interface provided by this one to add support for the desired IDs.

To do this, two files are needed:

1. `YourAddonName.omwscripts` with the following contents:

```
GLOBAL: scripts/YourAddonName/global.lua
```

1. `scripts/YourAddonName/global.lua` with the following contents:

```lua
local HL = require("openmw.interfaces").HarvestLights

if not HL then
    error("ERROR: Harvest Lights is not installed!")
end

HL.RegisterHarvestables({
        "havestable_id_01",
        "havestable_id_02",
        ...
})

HL.RegisterLights({
        "light_id_here_01",
        "light_id_here_02",
        ...
})
```

File layout:

```
.
├── YourAddonName.omwscripts
└── scripts
    └── YourAddonName
        └── global.lua
```

Note you should change `YourAddonName` to match your mod's name and the IDs used to match the IDs you want to add.

The `HL` variable gives you direct access to the Harvest Lights interface. Use `HL.RegisterHarvestables` to register harvestable IDs and `HL.RegisterLights` to register light IDs. You can use whatever script and path names you like, but it must be [a global script](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/overview.html#format-of-omwscripts).

Please see the included `example-addon` folder for a working example that adds all relevant [Tamriel_Data](https://www.nexusmods.com/morrowind/mods/44537) IDs as an add-on. Note that those IDs are supported out of the box and should not actually be used, it is only provided for educational purposes.

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/harvest-lights/-/issues)
* Email `harvest-lights@modding-openmw.com`
* Contact the author on Discord: `@johnnyhostile`
* Contact the author on Libera.chat IRC: `johnnyhostile`

#### Planned Features

* Support for existing saves (can't do this until the Lua API provides harvest data)
* When the OpenMW-Lua API gives access to the harvest status of objects, this mod's code that handles tracking that could be simplified.
