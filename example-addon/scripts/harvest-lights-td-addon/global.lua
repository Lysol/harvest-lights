local HL = require("openmw.interfaces").HarvestLights

if not HL then
    error("ERROR: Harvest Lights is not installed!")
end

HL.RegisterHarvestables(
    {
        "t_mw_flora_templedom01",
        "t_mw_flora_templedom02",
        "t_mw_flora_bloatspore01",
        "t_mw_floraow_bulbshroom_01",
        "t_mw_floraow_bulbshroom_02",
        "t_mw_floraow_bulbshroom_03",
        "t_mw_floraow_bulbshroom_04",
        "t_mw_floraow_bulbshroom_05",
        "t_mw_floraow_glwshrm_01",
        "t_mw_floraow_glwshrm_02",
        "t_mw_floraow_glwshrm_03",
        "t_mw_floraow_glwshrm_04",
        "t_mw_floraow_glwshrm_05",
        "t_mw_flora_yamorb01",
        "t_mw_flora_sheggshelf01",
        "t_mw_flora_sheggshelf02",
        "t_mw_flora_sheggshelf03"
    }
)

HL.RegisterLights(
    {
        "t_mw_light_aanthirinmushroom",
        "t_mw_light_bloatspore_128",
        "t_mw_light_bloatspore_512",
        "t_mw_light_bulbshroom_256",
        "t_mw_light_bulbshroom_1024",
        "t_mw_light_glowshrooms_128",
        "t_mw_light_glowshrooms_256",
        "t_mw_light_glowshrooms_512"
    }
)
