#!/bin/sh
set -e

modname=harvest-lights
file_name=$modname.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths ${file_name} \
    CHANGELOG.md \
    LICENSE \
    README.md \
    example-addon \
    l10n \
    scripts \
    $modname.omwscripts \
    version.txt

sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
