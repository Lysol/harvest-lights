if require('openmw.core').API_REVISION < 39 then
    error('This mod requires OpenMW 0.49.0 or newer, please update.')
end

return { MOD_ID = "HarvestLights", scriptVersion = 2 }
