local core = require('openmw.core')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local common = require("scripts.harvest-lights.common")
local harvested
local lights = {}

local function notifyShroom()
    local now = core.getGameTime()
    harvested = now
    core.sendGlobalEvent(
        "momw_hl_checkLightsForContainer",
        {
            items = nearby.items,
            theContainer = self,
            lights = lights,
            now = now
        }
    )
end

local function saveLight(data)
    table.insert(lights, data.light)
end

local function unHarvest()
    harvested = nil
    lights = {}
end

local function onActive()
    core.sendGlobalEvent(
        "momw_hl_onContainerActive",
        {
            items = nearby.items,
            theContainer = self,
            lights = lights,
            harvested = harvested
        }
    )
end

local function onLoad(data)
    if data.scriptVersion == 1 then
        harvested = data.harvested[self.id]
    else
        harvested = data.harvested
    end
    lights = data.lights
end

local function onSave()
    return {
        harvested = harvested,
        lights = lights,
        scriptVersion = common.scriptVersion
    }
end

return {
    engineHandlers = {
        onActive = onActive,
        onLoad = onLoad,
        onSave = onSave
    },
    eventHandlers = {
        momw_hl_notifyShroom = notifyShroom,
        momw_hl_saveLight = saveLight,
        momw_hl_unHarvest = unHarvest
    }
}
