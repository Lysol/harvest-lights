local core = require('openmw.core')
local storage = require('openmw.storage')
local time = require('openmw_aux.time')
local types = require('openmw.types')
local I = require("openmw.interfaces")
local common = require("scripts.harvest-lights.common")
local fourMonths = time.day * 120
local globalSettings = storage.globalSection('SettingsGlobal' .. common.MOD_ID)
local harvestableIds = {}
local lightIds = {}

I.Settings.registerGroup {
    key = 'SettingsGlobal' .. common.MOD_ID,
    page = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "globalSettingsTitle",
    permanentStorage = false,
    settings = {
        {
            key = 'locateRange',
            name = "locateRange_name",
            default = 200,
            renderer = 'number'
        },
        {
            key = 'extraRange',
            name = "extraRange_name",
            default = 20,
            renderer = 'number'
        }
    }
}

local function activationHandler(obj, actor)
    if harvestableIds[string.lower(obj.recordId)] then
        obj:sendEvent("momw_hl_notifyShroom")
    end
end

local function toggleMe(data)
    data.me.enabled = data.enabled
end

local function checkContainersForLight(data)
    local disableMe = true
    for _, thing in pairs(data.containers) do
        if harvestableIds[string.lower(thing.recordId)]
            and (data.theLight.position - thing.position):length() < globalSettings:get("locateRange")
            and not data.harvested[thing.id]
        then
            disableMe = false
        end
    end
    if disableMe then
        toggleMe({ me = data.theLight, enabled = false })
    end
end

-- Ran for a light
local function extraScan(data)
    local haveCloseContainers = false
    for _, thing in pairs(data.containers) do
        if harvestableIds[string.lower(thing.recordId)]
            and (data.this.position - thing.position):length() < globalSettings:get("locateRange")
            and not data.harvested[thing.id]
        then
            haveCloseContainers = true
        end
    end
    if not haveCloseContainers then
        data.originContainer:sendEvent("momw_hl_saveLight", {light = data.this})
        data.this:sendEvent("momw_hl_notifyLight", { obj = data.originContainer, time = data.now })
    end
end

local function checkLightsForContainer(data)
    for _, item in pairs(data.items) do
        if lightIds[string.lower(item.recordId)]
            and (data.theContainer.position - item.position):length() < globalSettings:get("locateRange")
        then
            data.theContainer:sendEvent("momw_hl_saveLight", {light = item})
            item:sendEvent("momw_hl_notifyLight", { obj = data.theContainer, time = data.now })
        elseif  lightIds[string.lower(item.recordId)]
            and (data.theContainer.position - item.position):length() < globalSettings:get("locateRange") + globalSettings:get("extraRange")
        then
            item:sendEvent("momw_hl_lightExtraScan", {originContainer = data.theContainer, time = data.now})
        end
    end
end

local function onContainerActive(data)
    if harvestableIds[string.lower(data.theContainer.recordId)] then
        if data.harvested and (core.getGameTime() - data.harvested) >= fourMonths then
            for _, light in pairs(data.lights) do
                light:sendEvent("momw_hl_notifyLight", { shroom = data.theContainer, enable = true })
            end
            data.theContainer:sendEvent("momw_hl_unHarvest")
        end
    end
end

local function registerIds(idsTable, ids, kind)
    if ids then
        for _, id in pairs(ids) do
            if kind then
                print(string.format("Adding %s via interface: %s", kind, id))
            end
            idsTable[string.lower(id)] = true
        end
    end
end

local function registerHarvestableIds(ids)
	registerIds(harvestableIds, ids, "harvestable")
end

local function registerLightIds(ids)
	registerIds(lightIds, ids, "light")
end

local function getHarvestableIds()
	return harvestableIds
end

local function getLightIds()
	return lightIds
end

if core.contentFiles.has("Morrowind.esm") then
    print("Registering support for Morrowind.esm")
    -- Self-register the base Morrowind IDs
    registerIds(
        harvestableIds,
        {
            "flora_bc_mushroom_01",
            "flora_bc_mushroom_02",
            "flora_bc_mushroom_03",
            "flora_bc_mushroom_04",
            "flora_bc_mushroom_05",
            "flora_bc_mushroom_06",
            "flora_bc_mushroom_07",
            "flora_bc_mushroom_08",
            "flora_bc_podplant_01",
            "flora_bc_podplant_02",
            "flora_bc_podplant_03",
            "flora_bc_podplant_04"
        }
    )
    registerIds(
        lightIds,
        {
            "bc mushroom 64",
            "bc mushroom 128",
            "bc mushroom 177",
            "bc mushroom 256",
            "orange_128_01_d",
            "orange_256_ci_02"
        }
    )
end

if core.contentFiles.has("Tamriel_Data.esm") then
    print("Registering support for Tamriel_Data.esm")
    registerIds(
        harvestableIds,
        {
            "t_mw_flora_templedom01",
            "t_mw_flora_templedom02",
            "t_mw_flora_bloatspore01",
            "t_mw_floraow_bulbshroom_01",
            "t_mw_floraow_bulbshroom_02",
            "t_mw_floraow_bulbshroom_03",
            "t_mw_floraow_bulbshroom_04",
            "t_mw_floraow_bulbshroom_05",
            "t_mw_floraow_glwshrm_01",
            "t_mw_floraow_glwshrm_02",
            "t_mw_floraow_glwshrm_03",
            "t_mw_floraow_glwshrm_04",
            "t_mw_floraow_glwshrm_05",
            "t_mw_flora_yamorb01",
            "t_mw_flora_sheggshelf01",
            "t_mw_flora_sheggshelf02",
            "t_mw_flora_sheggshelf03",
            "t_mw_flora_bluefoot01",
            "t_mw_flora_bluefoot02",
            "t_mw_flora_bluefoot03",
            "t_mw_flora_bluefoot04",
            "t_cyr_flora_wispstalk01",
            "t_sky_flora_rustruss01",
            "t_sky_flora_rustruss02",
            "t_sky_flora_rustruss03",
            "t_sky_flora_rustruss04",
            "t_sky_flora_rustruss05"
        }
    )
    registerIds(
        lightIds,
        {
            "t_mw_light_aanthirinmushroom",
            "t_mw_light_bloatspore_128",
            "t_mw_light_bloatspore_512",
            "t_mw_light_bulbshroom_256",
            "t_mw_light_bulbshroom_1024",
            "t_mw_light_glowshrooms_128",
            "t_mw_light_glowshrooms_256",
            "t_mw_light_glowshrooms_512",
            "t_glb_light_plant_128",
            "t_glb_light_plant_256",
            "t_glb_light_plant_512",
            "t_glb_light_plant_64",
            "t_glb_light_sheggoshelf_128",
            "t_glb_light_sheggoshelf_256",
            "t_glb_light_sheggoshelf_512",
            "t_glb_light_sheggoshelf_77",
            "t_glb_light_shroom1_128",
            "t_glb_light_shroom2_128",
            "t_glb_light_shroom3_128",
            "t_glb_light_shroom4_128",
            "t_glb_light_wispstalk_64"
        }
    )
end

I.Activation.addHandlerForType(types.Container, activationHandler)

return {
    eventHandlers = {
        momw_hl_checkContainersForLight = checkContainersForLight,
        momw_hl_checkLightsForContainer = checkLightsForContainer,
        momw_hl_globalExtraScan = extraScan,
        momw_hl_onContainerActive = onContainerActive,
        momw_hl_toggleMe = toggleMe
    },
    interfaceName = common.MOD_ID,
    interface = {
        GetHarvestables = getHarvestableIds,
        GetLights = getLightIds,
        RegisterHarvestables = registerHarvestableIds,
        RegisterLights = registerLightIds,
        version = 1
    }
}
