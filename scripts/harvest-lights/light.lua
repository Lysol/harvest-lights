local core = require('openmw.core')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local common = require("scripts.harvest-lights.common")
local harvested = {}

local function notifyLight(data)
    if data.enable then
        -- Triggered via harvestable onActive() handler
        -- to tell this light to be enabled.
        core.sendGlobalEvent(
            "momw_hl_toggleMe",
            { enabled = true, me = self }
        )
        harvested[data.shroom.id] = nil
        return
    end
    -- Triggered via player harvest
    local containers = nearby.containers
    if data.containers then
        containers = data.containers
    end
    harvested[data.obj.id] = data.time
    core.sendGlobalEvent(
        "momw_hl_checkContainersForLight",
        {
            containers = containers,
            theLight = self,
            harvested = harvested
        }
    )
end

local function extraScan(data)
    core.sendGlobalEvent(
        "momw_hl_globalExtraScan",
        {
            containers = nearby.containers,
            harvested = harvested,
            originContainer = data.originContainer,
            this = self,
            time = data.now
        }
    )
end

local function onLoad(data)
    harvested = data.harvested
end

local function onSave()
    return {
        harvested = harvested,
        scriptVersion = common.scriptVersion
    }
end

return {
    engineHandlers = {
        onLoad = onLoad,
        onSave = onSave
    },
    eventHandlers = {
        momw_hl_lightExtraScan = extraScan,
        momw_hl_notifyLight = notifyLight
    }
}
